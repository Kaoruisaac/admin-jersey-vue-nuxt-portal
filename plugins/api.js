import axios from 'axios';
import Cookies from 'js-cookie';
import EventBus from '~/plugins/EventBus';


const isDev = !(process.env.NODE_ENV === 'production');
const HostUrl = isDev?'http://localhost:8080/AdminJersey/api/':'http://localhost:8080/api/';


function AccountApi(){}
AccountApi.prototype = {
	GetAccountList(currentPage){
		let url = HostUrl+"account/getList";
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,{currentPage},header))
				.then(res=>{r(res)});
			})
		})
	},
	GetRoles(){
		let url = HostUrl+"account/getRoles";
		return new Promise((r,e)=>{
				this.ResultCheck(axios.get(url))
				.then(res=>{r(res)});
		})
	},
	AddAccount(req){
		let url = HostUrl+"account/add";
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,req,header))
				.then(res=>{r(res)});
			})
		})
	},
	GetAccount(){
		let url = HostUrl+"account/getInfo/";
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,{},header))
				.then(res=>{r(res)});
			})
		})
	},
	GetAccountById(uid){
		let url = HostUrl+"account/getInfo/"+uid;
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,{},header))
				.then(res=>{r(res)});
			})
		})
	},
	ModifyAccount(req){
		let url = HostUrl+"account/modify";
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,req,header))
				.then(res=>{r(res)});
			})
		})
	},
	DeleteAccount(uid){
		let url = HostUrl+"account/delete/"+uid;
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,{},header))
				.then(res=>{r(res)});
			})
		})
	}
}
function ArticleApi(){}
ArticleApi.prototype = {
	uploadImage(req){
		let url = HostUrl+"article/imageUpload";
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,req,header))
				.then(res=>{r(res)});
			})
		})
	},
	AddArticle(req){
		let url = HostUrl+"article/add";
		console.log(req);
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,req,header))
				.then(res=>{r(res)});
			})
		})
	},
	GetArticleList(currentPage){
		let url = HostUrl+"article/getList";
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,{currentPage},header))
				.then(res=>{r(res)});
			})
		})
	},
	GetArticleById(uid){
		let url = HostUrl+"article/get/"+uid;
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,{},header))
				.then(res=>{r(res)});
			})
		})
	},
	DeleteArticle(uid){
		let url = HostUrl+"article/delete/"+uid;
		return new Promise((r,e)=>{
			this.GetUserHeader().then((header)=>{
				this.ResultCheck(axios.post(url,{},header))
				.then(res=>{r(res)});
			})
		})
	}
}

class API{
	constructor(vue_instance){ 
		this.vi = vue_instance;
		this.HostUrl = HostUrl;
		return this; 
	}
	SetCookies(name,obj){
		return new Promise(r=>{
			Cookies.set(name,obj);
			r();
		}).then();
	}
	GetCookies(name){
		return new Promise(r=>{
			r(JSON.parse(Cookies.get(name)));
		}).then();	
	}
	RemoveCookies(name){
		return new Promise(r=>{
			r(Cookies.remove(name));
		}).then();	
	}
	SetUser(user){
		return this.SetCookies('user',user);
	}
	GetUser(){
		return this.GetCookies('user');
	}
	RemoveUser(){
		return this.RemoveCookies('user');
	}
	ResultCheck(promise){
		return promise.then(res=>{
			if(res.data.code!=1000){
				if(res.data.code==1004){
					EventBus.$emit("alert",{
						message:res.data.msg,
						callback:()=>{
							this.GoLoginPage();
						}});
				}else{
					EventBus.$emit("alert",{message:res.data.msg});
				}
			}
			return res;
		}).catch(()=>{
			EventBus.$emit("alert",{message:"Network Error !"});
		})
	}
	Login(req){
		let url = HostUrl+"account/login";
		return new Promise((r,e)=>{
			this.ResultCheck(axios.post(url,req))
			.then((res)=>{
				if(res.data.code==1000){
					this.SetUser(res.data.data);
				}
				r(res);
			}).catch(()=>{
				e();
			})
		})
	}
	GetUserHeader(){
		return new Promise((r,e)=>{
			this.GetUser().then(member=>{
				if(member){
					r({headers:{'Authorization':member.Token}})
				}else{
					e('Faild to get staff token !');
					this.goLoginPage();
				}
			})
		})  
	}
	GoLoginPage(){
		this.vi.router.push('/login');
	}
}

// inherent api
for(let key in AccountApi.prototype){
	API.prototype[key] = AccountApi.prototype[key];
}
for(let key in ArticleApi.prototype){
	API.prototype[key] = ArticleApi.prototype[key];
}

export default ({app},inject)=>{
	inject('api',new API(app));
}