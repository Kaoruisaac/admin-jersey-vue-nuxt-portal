import Vue from 'vue';
import MuseUI from 'muse-ui';
import Message from 'muse-ui-message';

Vue.use(Message);
Vue.use(MuseUI);